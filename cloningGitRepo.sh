echo "cloningGitRepo: Starting clone"

echo "Current location: "

pwd

cd public 

echo "Navigated location: "

pwd

git clone https://github.com/kumaravelqdm/core_boiler_palate.git $1

echo "Removing the git"

cd $1

pwd

rm -rf .git

echo "Removed git"

echo "cloningGitRepo: Completed cloning shell script"
